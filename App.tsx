import { StatusBar } from 'expo-status-bar';
import {Keyboard, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import HeaderRow from "./components/HeaderRow";
import CardsRow from "./components/CardsRow";
import {useState} from "react";

export default function App() {
    const [searchQuery, setSearchQuery] = useState('');
    const handleSearch = (query: string) => {
        setSearchQuery(query);
    }

  return (
        <View style={styles.container}>
            <HeaderRow onSearch={handleSearch} />
            <CardsRow queryInput={searchQuery} />
            {/*<StatusBar style="auto" />*/}
        </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
