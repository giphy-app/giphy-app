import React, {useEffect, useState} from 'react';
import {Button, FlatList, StyleSheet, Text, View} from "react-native";
import {API_GIPHY_KEY} from "../config";
import CardCol from "./CardCol";


type AppProps = {
    queryInput: string
}

const CardsRow = ({queryInput}: AppProps) => {
    const [images, setImages] = useState([]);
    const [limit, setLimit] = useState(15);
    const [offset, setOffset] = useState(30);
    const [rating, setRating] = useState('g');
    const [lang, setLang] = useState('en');

    const loadMore = () => {
        setOffset(prev => prev + 30);
    };

    useEffect(() => {

        setLimit(15);
        setOffset(30);
        fetch(`https://api.giphy.com/v1/gifs/search?api_key=${API_GIPHY_KEY}&q=${queryInput}&limit=${limit}&offset=${offset}&rating=${rating}&lang=${lang}`)
            .then((r) => {
                if (!r.ok) {
                    throw new Error('Network response was not ok');
                }
                return r.json();
            })
            .then((d) => {
                const result = d.data.map((item: { images: { original: { url: any; }; }; id: any; }) => {
                    return {id: item.id, url: item.images.original.url}

                });
                setImages(result);
            })
            .catch((e) => {
                console.log(e);
            })

    }, [queryInput]);


    useEffect(() => {
        fetch(`https://api.giphy.com/v1/gifs/search?api_key=${API_GIPHY_KEY}&q=${queryInput}&limit=${limit}&offset=${offset}&rating=${rating}&lang=${lang}`)
            .then((r) => {
                if (!r.ok) {
                    throw new Error('Network response was not ok');
                }
                return r.json();
            })
            .then((d) => {
                const result = d.data.map((item: { images: { original: { url: any; }; }; id: any; }) => {
                    return {id: item.id, url: item.images.original.url}

                });
                // @ts-ignore
                setImages(prevImages => [...prevImages, ...result]);
            })
            .catch((e) => {
                console.log(e);
            })

    }, [limit, offset]);

    return (
        <View style={styles.cardCon}>
            {/*<Button title={'Click'} onPress={test} />*/}
            <CardCol giphs={images} onLoadMore={loadMore} />
        </View>
    );
};

const styles = StyleSheet.create({
    cardCon: {
        flex: 1
    }
})

export default CardsRow;