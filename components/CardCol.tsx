import React, {useState} from 'react';
import {
    FlatList,
    Keyboard,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {Card} from 'react-native-paper';
import { FontAwesome } from '@expo/vector-icons';


type Giph = {
    id: number;
    url: string;
};

type CardColProps = {
    giphs: Giph[],
    onLoadMore: () => void,

}

const CardCol = ({giphs, onLoadMore}: CardColProps) => {

    const [numColumns, setNumColumns] = useState(2);
    const [showLoadMore, setShowLoadMore] = useState(true);


    const butttonComponent = () => {
        return (
            <View>
                    <View style={styles.loadMoreContainer}>
                        <TouchableOpacity style={styles.buttonLoadMore} onPress={onLoadMore}>
                            <Text style={styles.buttonText}>Load more</Text>
                        </TouchableOpacity>
                    </View>

            </View>
        );
    }


    return (
      <>
          {giphs.length === 0 ? (
              <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                  <View>
                      <View style={styles.iconContainer}>
                          <FontAwesome name="search" size={64} color="#9999ff" />
                      </View>
                  </View>
              </TouchableWithoutFeedback>
          ) : (
              <FlatList
                  data={giphs}
                  renderItem={({item, index}) => {
                      return (

                          <>
                              <View style={styles.cardBody}>
                                  <Card>
                                      <Card.Cover source={{uri: item.url}}/>
                                  </Card>
                              </View>
                          </>

                      );
                  }}
                  contentContainerStyle={styles.card}
                  numColumns={numColumns}
                  keyExtractor={(item) => item.id.toString()}
                  ListFooterComponent={() => butttonComponent()}
              />
          )
          }
      </>
    );

};

const styles = StyleSheet.create({
    card: {
        elevation: 4,
    },
    cardBody: {
        flex: 1,
        borderRadius: 25,
        margin: 8,
    },
    iconContainer: {
        width: '100%',
        height: '100%',
        textAlign: 'center',
        alignItems: 'center',
        borderRadius: 50,
        justifyContent: 'center'
    },
    loadMoreContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 16,
    },
    buttonLoadMore: {
        backgroundColor: '#9999ff',
        paddingVertical: 15,
        paddingHorizontal: 25,
        borderRadius: 10,
    },
    buttonText: {
        fontWeight: 'bold',
        color: 'white'
    }
})

export default CardCol;