import React, {useState} from 'react';
import {StyleSheet, Text, View} from "react-native";
import { Searchbar } from 'react-native-paper';



type HeaderRowProps = {
    onSearch: (query: string) => void;
}


const HeaderRow = ({onSearch}: HeaderRowProps) => {
    const [searchText, setSearchText] = useState('');


    return (
        <View style={styles.searchContainer}>
            <Searchbar
                placeholder="Search"
                onChangeText={(query) => {
                    setSearchText(query);
                    onSearch(query);
                }}
                value={searchText}
            />
        </View>
    );

};

const styles = StyleSheet.create({
    searchContainer: {
        paddingHorizontal: 20,
        paddingTop: 60,
        paddingBottom: 30,
        backgroundColor: '#9999ff',
        marginBottom: 0
    }
})

export default HeaderRow;